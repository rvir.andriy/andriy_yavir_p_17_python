import pandas as pd
import pymongo

def print_hello_world():
    print("Hello world")
def task1_1SumVals(val1, val2):
    print("Sum: {}+{}={}".format(val1, val2, val1+val2))
def task1_2PrintArgs(args):
    print("Args: {}".format(",".join(args)))
#*************************************************************
def createCsv(clients):
    df=pd.DataFrame(clients, columns=['id','first_name','last_name','email','gender'])
    df.to_csv('clients.csv',header=True, index=False)
def insertCsv(clients):
    df=pd.DataFrame(clients)
    df.to_csv('clients.csv',mode='a',header=False, index=False)
def printCsv():
    df=pd.read_csv('clients.csv')
    print(df.head())
def csvGetId(id):
    df = pd.read_csv('clients.csv')
    return df[(df['id']==id)]
def csvDelete(id):
    df = pd.read_csv('clients.csv')
    print(df.head())
    df1=df[(df['id']!=id)]
    df1.to_csv('clients.csv',header=True, index=False)
# **************************************************************
def createJson(clients):
    df=pd.DataFrame(clients, columns=['id','first_name','last_name','email','gender'])
    df.to_json('clients.json')
def insertJson(newclients):
    df=pd.read_json('clients.json')
    df1=pd.DataFrame(newclients)
    df2=df.append(df1,ignore_index=True)
    df2.to_json('clients.json')
def printJson():
    df=pd.read_json('clients.json')
    print(df.head())
def jsonGetId(id):
    df=pd.read_json('clients.json')
    return df[(df['id']==id)]
def jsonDelete(id):
    df=pd.read_json('clients.json')
    df1=df[(df['id']!=id)]
    df1.to_json('clients.json')
# **************************************************************
def createDB(dbname):
    return mngClient[dbname]
def createCollection(colname):
    return mydb[colname]
def insertCollection(mycol,newclients):
    for key in newclients:
        x = mycol.update(key,key,upsert=True)
def printCollection(mycol):
    ls=[]
    for x in mycol.find({},{"_id":0}):
        ls.append(x)
    df = pd.DataFrame(ls,columns=['id','first_name','last_name','email','gender'])
    print(df.head())
def documentGetId(mycol,id):
    ls=[]
    myquery = {"id": id}
    for x in mycol.find(myquery,{"_id":0}):
        ls.append(x)
    df = pd.DataFrame(ls, columns=['id', 'first_name', 'last_name', 'email', 'gender'])
    print(df.head())
    return ls
def documentDelete(mycol, id):
    myquery = {"id": id}
    mycol.delete_one(myquery)
def allDocumentDelete(mycol):
    x = mycol.delete_many({})
    print(x.deleted_count, " documents deleted.")
# **************************************************************
if __name__ == "__main__":
# 1. Write a function that will return a sum of two numbers, given as arguments.
# 2. Write a function that will print information about person (name, age etc.) that is given in arguments.
# 3. Call these functions from 'main'.
    print("********************\nTask1:")
    print_hello_world()
    task1_1SumVals(345,456)
    arr=["Andriy","29"]
    task1_2PrintArgs(arr)
# **************************************************************
    print("********************\nTask2:")
#2.1 Insert a client record into the file
#2.2 Read client data from file and print it in readable way
# 2.3 Get specific record from file (by id, for example)
#2.4 Delete record from file (by id)
# **************************************************************
# Work with csv:
    clientsList = {'id':[1,2,3],
                'first_name':['Derek','Steve','Paul'],
               'last_name':['lastN','lastN','lastN'],
               'email':['emaleDerek','SteveEmail','PaulEmail'],
               'gender':['male','male','male']}
    createCsv(clientsList)

    newXlientsList = {'id':[4,5],
                'first_name':['Andrew','John'],
               'last_name':['Fray','Nilsen'],
               'email':['emaleAndrew','JohnEmail'],
               'gender':['male','male']}
    insertCsv(newXlientsList)
    printCsv()
    print('Get row where id =5: \n {}'.format(csvGetId(4)))
    csvDelete(4)
#***************************************************************************
# Work with json:
    createJson(clientsList)
    insertJson(newXlientsList)
    printJson()
    print(jsonGetId(4))
    jsonDelete(4)
    printJson()
#********************************************************************
#Work with mongodb:
    listClients=[
        {'id': '1','first_name':'Derek','last_name':'lastN','email':'emaleDerek','gender':'male'},
        {'id':'2','first_name':'Steve','last_name':'lastN','email':'SteveEmail','gender':'male'},
        {'id':'3','first_name':'Paul','last_name':'lastN','email':'PaulEmail','gender':'male'}
    ]

    newXClientsList=[
        {'id': '4','first_name':'Andrew','last_name':'Fray','email':'emaleAndrew','gender':'male'},
        {'id': '5','first_name':'John','last_name':'Nilsen','email':'JohnEmail','gender':'male'}
    ]

    mngClient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = createDB("clients")
    mycol = mydb["client"]

    printCollection(mycol)
    allDocumentDelete(mycol)
    printCollection(mycol)
    insertCollection(mycol,listClients)
    insertCollection(mycol,newXClientsList)
    printCollection(mycol)
    documentDelete(mycol,'4')
    printCollection(mycol)
    print(documentGetId(mycol,'5'))

    print("********************")



